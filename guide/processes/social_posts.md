---
title: Posting to Social Media
weight: 6
menu:
  sidebar:
    name: "Social Media Posts"
    parent: "Processes"
---

## Channels

### Official Inkscape Channels

| Platform    | Current main contact(s) |
| ----------- | ----------------------- |
| [Facebook](https://www.facebook.com/Inkscape.org) | Maren (@Moini) |
| [X (Twitter)](https://twitter.com/inkscape/) | Mihaela (@prkos), Adam (@adam.belis) |
| [Mastodon](https://mastodon.art/@inkscape) | Maren (@Moini) |
| [Instagram](https://www.instagram.com/inkscapeofficial/) | Maren (@Moini) |
| [DeviantArt](https://www.deviantart.com/inkscapers) | @m1981 |
| [Forums](https://inkscape.org/forums) | @ltlnx |
| [Bluesky - in planning stage] | @Greenfoot5 |


### Channels Managed by Friends

| Platform    | Current main contact(s) |
| ----------- | ----------------------- |
| [Facebook Group](https://www.facebook.com/groups/inkscape.org) | Chris (@CRogers), Sonia (@Sonia) |
| [Gimpscape Indonesia](https://gimpscape.or.id/) | Rania (@raniaamina) |
| [German Facebook Group](https://www.facebook.com/groups/258257451374594/) | Jörg (@Joerg) |
| [Discord Server](https://discord.gg/D7C4bXh4BM) | Greenfoot (@Greenfoot5) |

### Social Media Tag List

This is a list of tags to reach people for posting our news to social media, using their rocket.chat names for [chat.inkscape.org](https://chat.inkscape.org):

`@prkos @Moini @CRogers @Sonia @raniaamina @Joerg @Greenfoot5 @adam.belis @ltlnx @m1981`

## Posting Frequency

TBD

## Post Contents

### Components of a Good Social Media Post

Optimally, each social media post should consist of the following elements:

* A text with the contents (length limits: X: 280, Mastodon: 500)
* An image, gif animation or short video
* An image description (alt text)

In some cases, graphics may be expendable.

### Post Topics

These are some examples of topics for social media posts:

* New Inkscape version released
* News article published
* Call for mentees (e.g. GSoC, Outreachy, …)
* Event info (e.g. updates on status of About Screen Contest, Inkscape Summits, events the project attends, events the project organizes)
* Donation requests (either for Inkscape or for SFC)
* User surveys (e.g. from UX team)
* Call for contributions (e.g. bug triage, testing beta version, …)
* Inkscape milestones (e.g. cool new feature landed in main branch, switch to new GTK version, new contractor, …)

### Post Sharing Policy

Some of the above platforms support sharing other users' contents from the official Inkscape channels.

We can share:

* TBD

We will not share:

* NSFW contents / non-family-friendly contents
* Political contents
* Spam / Scam contents
* Clickbait
* Polarized discussions
* …

We will try to avoid sharing:

* Contents by dubious accounts (accounts who share a lot of NSFW contents / propaganda / conspiracy theories) that we do not want Inkscape to be deemed associated with
* Low-quality contents
* …


