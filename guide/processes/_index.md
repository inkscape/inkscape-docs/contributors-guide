---
title: Processes
chapter: true
weight: 3
menu: 
  sidebar:
    name: "Processes"
---

# Processes

Some processes in the Inkscape Community require a lot of 'things to remember'. For your convenience, find some of our processes listed in this guide.
