---
title: Project Structure
weight: 3
menu:
  sidebar:
    name: "Project Structure"
    parent: "Project Fundamentals"
---

## Teams

### Developers

#### macOS Developers

#### Extension Developers

### Documentation

### Testing & Bug Management

### Translators

#### Website Translators

### UI / UX

### Vectors

#### Social Media Managers

### Website

## Projects

[Full list](https://gitlab.com/inkscape)



